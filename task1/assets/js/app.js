'use strict';

window.onload = function(e) {
  let form   = document.querySelector('.form');
  let square = document.querySelector('.square');

  form.addEventListener('submit', function(e) {
    e.preventDefault();

    getJSON('/data/test.json')
      .then(json => count(json))
      .catch(error => console.log(error));
  });
}

function count(json) {
  const { expenditure, materials, prices } = json;
  const volume = Math.ceil(square.value * expenditure);

  let cheapProducts = {};
  let result = {
    productID:       '',
    provider: '',
    price:    ''
  };

  for(let productID in prices) {
    var tmpProduct = {
      id:       '',
      provider: '',
      price:    ''
    };

    var price = prices[productID];

    for(let providerID in price) {
      if(price[providerID] < tmpProduct.price || tmpProduct.price === '') {
        tmpProduct.id       = productID;
        tmpProduct.provider = providerID;
        tmpProduct.price    = price[providerID];
      }
    }

    cheapProducts[tmpProduct.id] = {
      provider: tmpProduct.provider,
      price:    tmpProduct.price
    };
  }

  for(let productID in cheapProducts) {
    let price;
    let cheapProduct = cheapProducts[productID];
    let remain = volume % materials[productID]; // вычисляем остаток

    if (remain === 0) {
      let count = volume / materials[productID];

      price = count * cheapProduct.price;
    } else {
      if (volume / materials[productID] > 1) {
        let count = (volume - remain) / materials[productID]; // получаем колличество без остатка и цену этого кол-ва

        price = count * cheapProduct.price;

        // минимальный обьем упаковок
        let min = [];
        for(let p_productID in prices) {
          let p_price = prices[p_productID];
          if (p_price[cheapProduct.provider]) min.push(materials[p_productID]);
        }
        min = Math.min.apply(null, min);

        // выясняем цену остатка и плюсуем ее к сумме
        if (remain <= min) {
          price += prices[productID][cheapProduct.provider];
        } else {
          // остальные варианты
          for(let m_productID in materials) {
            if (remain === materials[m_productID]) {
              price += prices[m_productID][cheapProduct.provider];
            } else {
              let tmpRemain = remain % materials[m_productID];
              let count = (remain - tmpRemain) / materials[m_productID];

              if (tmpRemain < materials[m_productID]) count++;

              price += prices[m_productID][cheapProduct.provider] * count;
            }
          }
        }
      }
    }

    // Сравниваем цены
    if(price < result.price || result.price === '') {
      result.productID = productID;
      result.price     = price;
      result.provider  = cheapProduct.provider;
    }
  }

  localStorage.setItem('result', JSON.stringify(result));
}

function getJSON(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', url, true);

    xhr.onload = function() {
      if (this.status === 200) {
        const json = JSON.parse(this.response);
        return resolve(json);
      } else {
        let error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    }

    xhr.onerror = function() {
      reject(new Error('Network Error'));
    };

    xhr.send();
  });
}